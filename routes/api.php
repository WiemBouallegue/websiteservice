<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

use App\Http\Controllers\RobberyController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




/*Route::get('test', function(){
	$firstname=Input::get("firstname");
	$lastname=Input::get("lastname");
	$birth=Input::get("birth");
	list($month, $day, $year)=explode("-", $birth);
	$age=Carbon::createFromDate($year, $month, $day)->diff(Carbon::now())->format('%y years, %m months and %d days');
	$fullname=$firstname.$lastname." ".$age;
    return ['fullname' =>$fullname,
    		'age'=>$age] ;
});	*/

/*Route::get('test', function(){
	$client = new \GuzzleHttp\Client();
$res = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');
echo $res->getStatusCode();
});*/
Route::post('test', 'RobberyController@robberyTookPlace');

/*Route::post('test', function(){
	$data = base64_encode(file_get_contents( $_FILES["photo"]["tmp_name"] ));
	
	$client = new Client();
	$r = $client->request('POST', 'https://vision.googleapis.com/v1/images:annotate?key=AIzaSyDtHCtSeeyWnX7Q9LOlMU0uTZAOxVPzQ5s', [
    'body' => '{
  "requests":[
    {
      "image":{
      	     
        "content": "'.$data.'"
      },
      "features":[
        {
          "type":"WEB_DETECTION",
          "maxResults":10
        }
      ]
    }
  ]
}

'
]);

$body = $r->getBody();
$array = json_decode($r->getBody(), true);
$listRobbery=array("Robbery", "Theft", "Bank robbery", "Crime", "Knife", "Arrest", "Hood", "Weapon", "Firearm", "Gun", "Hammer", "Fight", "Extortion");

$size=count($array['responses'][0]['webDetection']['webEntities']);
$exist="false";
$description="";
for($i=0; $i<$size ;$i++){
	$word=$array['responses'][0]['webDetection']['webEntities'][$i]['description'];
	if(in_array($word, $listRobbery)){
		$exist="true";
		if($description==""){
		$description=$word;
		}
		else
		$description=$description.', '.$word;	
	}
}
$jsonReturn=array('Robbery' => $exist, 'Description' => $description);
echo json_encode($jsonReturn);
});*/


